<?php 
/*
 * 应用中心主页：http://addon.discuz.com/?@ailab
 * 人工智能实验室：Discuz!应用中心十大优秀开发者！
 * 插件定制 联系QQ594941227
 * From www.ailab.cn
 */
 
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class jsonapi_userlist{
	public static $_api='userlist';
	public static $_name='用户列表';
	public static $_info='获得论坛用户列表的api，可以使用分页方式多次请求！';
	public static $_cachekey='jsonapi_userlist_config';
	public static $_keylist=array(
		'uid'=>'用户uid',
		'username'=>'用户名',
		'email'=>'用户邮箱',
	
	);
	
	public $_config;
	public $_pagenum=20;
	
	function __construct(){
		global $_G;	
		$this->getConfig();
	}
	
	function getConfig(){
		global $_G;
		loadcache(self::$_cachekey);
		$this->_config=$_G['cache'][self::$_cachekey];
	}
	
	function getData($where='',$orderby='',$sort='',$limit=''){
		if(!$this->_config['return']) return array('data'=>array(),'allcount'=>0);
		else{
			$retrun=implode(',',array_keys($this->_config['return']));
			$allcount=DB::result_first("select count(*) from ".DB::table('common_member')." where 1 $where ");
			$data=DB::fetch_all("select $retrun from ".DB::table('common_member')." where 1 $where $orderby $sort $limit");
			return array('data'=>$data,'allcount'=>$allcount);
		}
	}
}



?>